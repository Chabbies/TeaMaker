#include <Servo.h>
#include <stdbool.h>
#include <SPI.h>
#include <Ethernet.h>
#include <String.h>
#include <stdlib.h>
#include <Math.h>
#include "Melody.h"

// Lid Control //
Servo lidServo;
int lidPin = 7;

// Relay Control //
int relayPin = 2;

// Leaves Control //
Servo leavesServo;
int leavesServoPin = 8;
bool useLeaves1 = false;
bool useLeaves2 = false;
int L_SERVO_ORIGIN_POS = 100;
int L_SERVO_POS1 = 20;
int L_SERVO_POS2 = 180;

// Process Start Control //
bool startProcess = false;
int timeOfBrew = 0;
int currentTime = 0;

// Ethernet Control //
int redLightPin = 3;//indicates that a connection failed
int greenLightPin = 4;//indicates that a connection succeeded and a response was received
int blueLightPin = 5;//indicates the instructions have changed

String jsonResponse = "";

//assign a mac address for the ethernet controller
byte macAdress[] = {
  0xDE, 0xAD, 0xBB, 0xFF, 0x30, 0xED
};
//this is an available ip address on my network
IPAddress ip(192, 168, 1, 5);
//Domain Name Server address:
IPAddress myDns(192, 168, 1, 1);
//intialize ethernet client
EthernetClient ethClient;
char server[] = "www.harrydong.com";

unsigned long lastConnectionTime = 0;             // last time you connected to the server, in milliseconds
const unsigned long postingInterval = 10L * 1000L; // delay between updates, in milliseconds
// the "L" is needed to use long type numbers

void httpRequest() {
  /* This function makes a HTTP connection to the server */
  // Close any connection before send a new request: (This will free the socket on the WiFi shield)
  ethClient.stop();

  // if there's a successful connection:
  if (ethClient.connect(server, 80)) {
    digitalWrite(redLightPin, LOW);
    Serial.println("connecting...");
    // send the HTTP GET request:
    ethClient.println("GET /teadaddy.php HTTP/1.1");
    ethClient.println("Host: www.harrydong.com");
    ethClient.println("User-Agent: arduino-ethernet");
    ethClient.println("Connection: close");
    ethClient.println();

    //record time of last connection
    lastConnectionTime = millis();
  }
  else {
    //if connection fails, light the red light
    Serial.println("connection failed");
    digitalWrite(redLightPin, HIGH);
  }
}

int parseJSONData(String str){
  /* This function updates the global variables timeOfBrew and currentTime */
  //converting str to char array
  Serial.println("Starting parsing...");
  int str_len = str.length() + 1;
  char c[str_len];
  str.toCharArray(c, str_len);
  int count = 1;
  currentTime = 0;
  timeOfBrew = 0;
  //parse jsoninput int temp char a, splits every time encounters one of those symbols
  char* a = strtok(c, "\":{},");
  while (a != NULL){
    //Serial.println(a);
    if (!strcmp(a, "black")) useLeaves1 = true;
    if (!strcmp(a, "green")) useLeaves2 = true;
    if (!strcmp(a, "00")) count--;
    if (atoi(a) > 0) {
      while (count >= -1){
        currentTime += (atoi(a) * pow(60, count));
        count--;
        a = strtok(NULL, "\":{},");
      }
      if (count == -2 && timeOfBrew == 0) {
        timeOfBrew = currentTime;
        currentTime = 0;
        count = 1;
      } //converting the timeOfBrew and currentTime into seconds and storing in variables
    }
    else{
      a = strtok(NULL, "\":{},");
    }
  }
  currentTime++;
  timeOfBrew++;
  Serial.println("Done parsing...current values of currentTime and timeOfBrew:");
  Serial.println(currentTime);
  Serial.println(timeOfBrew);
}

// Music Control //
// notes in the melody:
int melody[] = {
  NOTE_D4, NOTE_E4, NOTE_F4, NOTE_G4, NOTE_A4,
  NOTE_G4, NOTE_F4, NOTE_E4, NOTE_F4, NOTE_G4, NOTE_A4, NOTE_G4,
  NOTE_F4, NOTE_G4, NOTE_A4, NOTE_G4, NOTE_F4, NOTE_E4, NOTE_F4,
  NOTE_E4, NOTE_D4, NOTE_E4, NOTE_C4, NOTE_D4,

  NOTE_D4, NOTE_E4, NOTE_F4, NOTE_E4, NOTE_F4,
  NOTE_G4, NOTE_F4, NOTE_G4, NOTE_A4, NOTE_G4, NOTE_F4, NOTE_D4,
  NOTE_D4, NOTE_E4, NOTE_F4, NOTE_G4, NOTE_A4, NOTE_AS4, NOTE_D4,
  NOTE_G4, NOTE_F4, NOTE_G4, NOTE_E4, NOTE_D4
};

// note durations: 4 = quarter note, 8 = eighth note, etc.:
int noteDurations[] = {
  4,4,3,8,4,4,4,4,4,4,4,2,8,8,3,8,4,4,4,4,3,8,4,2,
  8,8,3,8,4,4,4,4,4,4,4,2,8,8,4,4,4,4,4,4,3,8,4,2
};

void playMelody(){
  for (int thisNote = 0; thisNote < 48; thisNote++) {
    // to calculate the note duration, take one second divided by the note type.
    //e.g. quarter note = 1000 / 4, eighth note = 1000/8, etc.
    int noteDuration = 1500 / noteDurations[thisNote];
    tone(6, melody[thisNote], noteDuration);

    // to distinguish the notes, set a minimum time between them.
    // the note's duration + 30% seems to work well:
    int pauseBetweenNotes = noteDuration * 1.30;
    delay(pauseBetweenNotes);
    // stop the tone playing:
    noTone(6);
  }
}

///////////////////////////////////////////////////////////////////////////////
void setup() {
  pinMode(relayPin, OUTPUT);
  digitalWrite(relayPin, HIGH);

  // start serial port:
  Serial.begin(9600);
  while (!Serial) {
      ; // wait for serial port to connect. Needed for native USB port only
  }

  //set up lights
  pinMode(redLightPin, OUTPUT);
  pinMode(greenLightPin, OUTPUT);
  pinMode(blueLightPin, OUTPUT);

  //startup feedback and giving the ethernet module time to boot up:
  digitalWrite(redLightPin, HIGH);
  delay(200);
  digitalWrite(greenLightPin, HIGH);
  delay(200);
  digitalWrite(blueLightPin, HIGH);
  delay(600);
  digitalWrite(redLightPin, LOW);
  digitalWrite(greenLightPin, LOW);
  digitalWrite(blueLightPin, LOW);

  // start the Ethernet connection using a fixed IP address and DNS server:
  Ethernet.begin(macAdress, ip, myDns);
  // print the Ethernet board/shield's IP address:
  Serial.print("My IP address: ");
  Serial.println(Ethernet.localIP());

  lastConnectionTime = millis();
}

/////////////////////////////////////////////////////////////////////////
void loop() {
  if (startProcess)
  {
    // Heat for 2 minutes:
    Serial.println("Heating...");
    digitalWrite(relayPin, LOW);
    delay(120000);
    digitalWrite(relayPin, HIGH);

    // Dispense Tea:
    Serial.println("Dispensing tea...");
    lidServo.attach(lidPin);
    delay(3000);

    leavesServo.attach(leavesServoPin);
    leavesServo.write(L_SERVO_ORIGIN_POS);
    delay(2000);

    if (useLeaves1){
      for(int _ =0; _ <5; _++){
        leavesServo.write(L_SERVO_POS1);
        delay(2000);
        leavesServo.write(L_SERVO_ORIGIN_POS);
        delay(1000);
      }
    }
    if (useLeaves2){
      for(int _ =0; _ <5; _++){
        leavesServo.write(L_SERVO_POS2);
        delay(2000);
        leavesServo.write(L_SERVO_ORIGIN_POS);
        delay(1000);
      }
    }
    // Close the process:
    Serial.println("Closing lid...");
    leavesServo.write(100);
    delay(2000);
    lidServo.detach();

    leavesServo.detach();

    startProcess = false;
    Serial.println("Brewing...");
    delay(180000); //Wait 3 minutes for tea to brew
    playMelody();
    Serial.println("Process done.");
  }

  else
  {
    if (ethClient.available()) {// if there's incoming data from the net connection, read it

      //green light indicates that a message was received successfully
      digitalWrite(greenLightPin, HIGH);
      int bracketsCount = 0;
      String currentResponse = "";

      while (ethClient.available()){//iterate over the ethClient response one character at a time
        char c = ethClient.read();
        if (c == '{'){//count open brackets to check for start of json array
          bracketsCount++;
        }
        if (bracketsCount){//append to json response
          currentResponse += c;
        }
        if (c == '}'){//count close brackets
          bracketsCount--;
        }
      }

      //check if instructions have changed (omits current time since current time should change every second)
      if (jsonResponse.indexOf(currentResponse.substring(0, currentResponse.indexOf("currentTime"))) < 0){
        Serial.println("instructions updated");
        digitalWrite(greenLightPin, LOW);

        //blue light indicates instructions have changed
        digitalWrite(blueLightPin, HIGH);
        delay(500);
        digitalWrite(blueLightPin, LOW);
      }

      //store latest response as String in jsonResponse
      jsonResponse = currentResponse;
      Serial.print(jsonResponse);
      Serial.println();
      digitalWrite(greenLightPin, LOW);
      //Check if process starts:
      if (jsonResponse != ""){

        parseJSONData(jsonResponse);

        //The + 1439 is to handle the case when time rotates i.e 23:59:00 to 00:00:00
        if (currentTime < timeOfBrew) currentTime += 1439;
        if (currentTime - timeOfBrew <= 3){
          startProcess = true;
          Serial.println("Starting process...");
        }

      }
    }
    //if a period of postingInterval has passed, then connect again and send data
    if (millis() - lastConnectionTime > postingInterval){
      Serial.println("Making http request...");
      httpRequest();
    }
  }
  //Serial.println("Loop");
}