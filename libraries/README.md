# Creating libraries for the Arduino
To create libraries, you must create a folder **\<Library\>** containing:

    1. <Library>.h -> Your header file
    2. <Library>.cpp -> Your code

Then you must place **\<Library\>** inside **Documents/Arduino/libraries**.
You should then be able to load them inside the IDE with **Sketch->Include Library**

**NOTE**: The module must be in **.cpp** or else you must put the header file inside an ugly wrapper, check out https://stackoverflow.com/questions/19635007/arduino-c-undefined-reference-to-readarms for more info.
