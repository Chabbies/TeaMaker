# TeaDaddy/A.T.Maker
The **TeaDaddy** is an automatic tea maker built for the **UW SE101** project.
## Features
- [x] Online configuration for:
    - [x] Brewing times
    - [x] Tea types
- [x] Automatic water heating
- [x] Automatic tea brewing
- [x] User notification on finish

## Navigating the repo
- Arduino component of TeaDaddy in **main/main.ino**
- Online component of TeaDaddy in **web/**
- Self-written modules in **libraries/**
- Project documents in **docs/**