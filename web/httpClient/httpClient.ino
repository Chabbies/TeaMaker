#include <SPI.h>
#include <Ethernet.h>
#include <String.h>

int redLight = 3;//indicates that a connection failed
int greenLight = 4;//indicates that a connection succeeded and a response was received
int blueLight = 5;//indicates the instructions have changed

String jsonResponse = "";

//assign a MAC address for the ethernet controller
byte mac[] = {
  0xDE, 0xAD, 0xBB, 0xFF, 0x30, 0xED
};
//this is an available ip address on my network
IPAddress ip(192, 168, 1, 5);
//Domain Name Server address:
IPAddress myDns(192, 168, 1, 1);
//intialize ethernet client
EthernetClient client;
char server[] = "www.harrydong.com";

unsigned long lastConnectionTime = 0;             // last time you connected to the server, in milliseconds
const unsigned long postingInterval = 10L * 1000L; // delay between updates, in milliseconds
// the "L" is needed to use long type numbers

void setup() {
    // start serial port:
    Serial.begin(9600);
    while (!Serial) {
        ; // wait for serial port to connect. Needed for native USB port only
    }

    //set up lights
    pinMode(redLight, OUTPUT);
    pinMode(greenLight, OUTPUT);
    pinMode(blueLight, OUTPUT);

    //startup feedback and giving the ethernet module time to boot up:
    digitalWrite(redLight, HIGH);
    delay(200);
    digitalWrite(greenLight, HIGH);
    delay(200);
    digitalWrite(blueLight, HIGH);
    delay(600);
    digitalWrite(redLight, LOW);
    digitalWrite(greenLight, LOW);
    digitalWrite(blueLight, LOW);

    // start the Ethernet connection using a fixed IP address and DNS server:
    Ethernet.begin(mac, ip, myDns);
    // print the Ethernet board/shield's IP address:
    Serial.print("My IP address: ");
    Serial.println(Ethernet.localIP());
}

void loop() {
    
    if (client.available()) {// if there's incoming data from the net connection, read it

        //green light indicates that a message was received successfully
        digitalWrite(greenLight, HIGH);
        int start = 0;//counts brackets
        String currentResponse = "";

        while (client.available()){//iterate over the client response one character at a time
            char c = client.read();
            if (c == '{'){//count open brackets to check for start of json array
                start++;
            }
            if (start){//append to json response
                currentResponse += c;
            }
            if (c == '}'){//count close brackets
                start--;
            }
        }
        
        //check if instructions have changed (omits current time since current time should change every second)
        if (jsonResponse.indexOf(currentResponse.substring(0, currentResponse.indexOf("currentTime"))) < 0){
            Serial.println("instructions updated");
            digitalWrite(greenLight, LOW);

            //blue light indicates instructions have changed
            digitalWrite(blueLight, HIGH);
            delay(500);
            digitalWrite(blueLight, LOW);
        }
                
        //store latest response as String in jsonResponse
        jsonResponse = currentResponse;
        Serial.print(jsonResponse);
        Serial.println();
        digitalWrite(greenLight, LOW);
    }

    //if a period of postingInterval has passed, then connect again and send data
    if (millis() - lastConnectionTime > postingInterval){
        httpRequest();
    }
}


void httpRequest() {//method makes a HTTP connection to the server:
    // close any connection before send a new request.
    // This will free the socket on the WiFi shield
    client.stop();

    // if there's a successful connection:
    if (client.connect(server, 80)) {
        digitalWrite(redLight, LOW);
        Serial.println("connecting...");
        // send the HTTP GET request:
        client.println("GET /teadaddy.php HTTP/1.1");
        client.println("Host: www.harrydong.com");
        client.println("User-Agent: arduino-ethernet");
        client.println("Connection: close");
        client.println();

        //record time of last connection
        lastConnectionTime = millis();
    }
    else {
        //if connection fails, light the red light
        Serial.println("connection failed");
        digitalWrite(redLight, HIGH);
    }
}
